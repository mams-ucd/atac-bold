# ATAC 2021 Submission: Building Management using the Semantic Web and Hypermedia Agents

Project for [ATAC-2021](https://all-agents-challenge.github.io/atac2021/): submission from Eoin O’Neill, Katharine Beaumont, Nestor Velasco Bermeo, Rem Collier of University College Dublin, Ireland.

## Video

[The demo video is here](https://youtu.be/uriJT-kAVMg). It has subtitles for your convenience.

## Requirements

You will need:

- [Java 1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) or later
- [Apache Maven](https://maven.apache.org/download.cgi/)
  - The latest versions of Maven 3.8.x cannot be used as this version blocks HTTP repository requests as part of their improved security requirements.
  - The JACAMO repository does not currently meet these requirements (i.e. it uses only HTTP).
  - This project has been tested with Maven 3.5.4 

## Running the code

- Download the BOLD server from [here](https://github.com/bold-benchmark/bold-server)
  - for ease, you can use the pre-compiled version of the BOLD server, which can be found [here](https://github.com/bold-benchmark/bold-server/releases/tag/v0.1.0) 
  - this version is configured to run at 100ms per iteration.
  - there is an issue with configuration - we have noticed that on WSL2, where hypervisor containers are used, BOLD binds to the container IP address. BOLD server assumes it binds to localhost (or the associated IP address). Any prewritten turtle provided (e.g. as part of the PUT request to start the simulation) must be modified to reflect the container IP address rather than localhost.
- Run the BOLD server and navigate to http://localhost:8080/index.html
  - Again, on some platforms (e.g. WSL2), this may be the container IP address
  - If required, modify the sim and start url initial beliefs in Main.astra to point to the start uri of the deployed BOLD server
  - compile and run the agents using Maven ( we type "mvn" in the atac-bold folder)

Note: the intials beliefs in the Main.astra file look like this:
```
    initial url("sim", "http://127.0.1.1:8080/sim");
    initial url("start", "http://127.0.1.1:8080/Building_B3");
```
Simply replace 127.0.1.1 with the relevant IP address.

Please contact [Rem Collier](mailto:rem.collier@ucd.ie) if additional information is needed.

## Viewing the code

- This code contains all the application specific code for the submission
- It also uses parts of of our Multi-Agent Microservices (MAMS) CArtAgO implementation, which can be found [here](https://gitlab.com/mams-ucd/mams-cartago)
 - The [mams-astra-jena](https://gitlab.com/mams-ucd/mams-cartago/-/tree/master/mams-astra-jena) git module has been developed explicitly for ATAC 2021.
 - It provides the Apache Jena / ASTRA integration, support for generating turtle code, and support for reasoning using RDFSchema.
 - This submission also makes use of pre-existing support in MAMS for performing PUT requests. This code can be found in the [mams-astra-hal](https://gitlab.com/mams-ucd/mams-cartago/-/tree/master/mams-astra-hal) git module.

## Paper

[The paper submission is here](https://gitlab.com/mams-ucd/atac-bold/-/blob/main/ATAC_Bold_Submission.pdf)

## Architecture diagram

![DT](/images/Sys diag.png)
